* For code, [GPLv3](licenses/LICENSE_FOR_CODE.md)
* For content, [CC-BY-SA 4.0](licenses/LICENSE_FOR_CONTENT.md)
* For contributors, see [DCO](licenses/DCO.txt)
